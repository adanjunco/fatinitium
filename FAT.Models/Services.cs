﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FAT.Models
{
    public class Services
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Ingrese nombre para cabecera del servicio")]
        [Display(Name = "Cabecera")]
        public string header { get; set; }

        [Required(ErrorMessage = "Ingrese titulo del servicio")]
        [Display(Name = "Titulo")]
        public string tittle { get; set; }

        [Required(ErrorMessage = "Ingrese descripcion del servicio")]
        [Display(Name = "Descripcion")]
        public string description { get; set; }

    }
}
