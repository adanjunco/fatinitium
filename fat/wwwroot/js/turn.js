﻿/*tittle service*/
$('#service').append(`<label>Turno para: ${tservice}</label>`);

/*Connect signal R*/
var connection = new signalR.HubConnectionBuilder().withUrl("/hub").build();
connection.start().then(function () {
});


function process_turn() {
    connection.invoke("NuevoTurno").catch(function (err) {
        return console.error(err.toString());
    });
}

var dataTable;


$(document).ready(function () {
    loadDataTable();
});


function loadDataTable() {
    dataTable = $("#table_turns").DataTable({
        
        "columns": [
            {"width": "35%", className: "table-light"}
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "sProcessing": "Procesando...",
        },
        "width": "100%"
    });
}

