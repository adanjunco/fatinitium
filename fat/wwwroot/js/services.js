﻿var dataTable;


$(document).ready(function () {
    loadDataTable();
});


function loadDataTable() {
    dataTable = $("#table_services").DataTable({
        "ajax": {
            "url": "/admin/services/GetAll",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "id", "width": "5%", className: "table-light" },
            { "data": "header", "width": "15%", className: "table-primary" },
            { "data": "tittle", "width": "15%", className: "table-light" },
            { "data": "description", "width": "40%", className: "table-primary" },
            {
                "data": "id",
                "render": function (data) {
                    return `<div class="text-center">
                            <a href='/Admin/Services/Edit/${data}' class='btn btn-primary text' style='cursor: pointer; width: 100px;'>
                            <i class='fas fa-edit'></i> Editar
                            </a>
                            &nbsp;
                            <a onclick=Delete("/Admin/Services/Delete/${data}") class='btn btn-danger text' style='cursor: pointer; width: 100px;'>
                            <i class='fas fa-edit'></i> Borrar  
                            </a>
                            `;
                }, "width": "35%", className: "table-light"
            }
        ],
        "language": {
           "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "sProcessing": "Procesando...",
        },
        "width": "100%"
    });
}


function Delete(url) {
    swal({
        title: "¿Esta seguro (a) de borrar?",
        text: "Una vez borrado no podra recuperar este servicio",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#375a7f",
        confirmButtonText: "Aceptar",
        closeOnconfirm: true
    },
        function (){
            $.ajax({
                type: 'DELETE',
                url: url,
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            });
        });
}


$("#tittle").append("<h2>`este es el titulo: '${tittle}'`</h2>");


