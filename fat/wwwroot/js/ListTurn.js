﻿//ListTurn js

// remove non-printable and other non-valid JSON chars
l = l.replace(/\\n/g, "\\n")
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f")
    .replace(/[\u0000-\u0019]+/g, "");
let list = JSON.parse(l.replace(/&quot;/g, '"'));
if (list.length > 0) {
    for (let i = 0; i < list.length; i++) {
        var tr = $('<tr/>');
        tr.append("<td>" + list[i].cliid + "</td>");
        tr.append("<td>" + list[i].cliente + "</td>");
        tr.append("<td>" + list[i].tipo_servicio + "</td>");
        tr.append("<td>" + list[i].cola + "</td>");
        tr.append("<td>" + list[i].posicion + "</td>");
        tr.append("<td>" + list[i].status + "</td>");
        tr.append("<td>" + list[i].tiempo_espera + "</td>");
        $('#table_turns tbody#list_table_body').append(tr);
    }
} else {
    var tr = $('<tr/>');
    tr.append('<td colspan="4">No hay data</td>');
    $('#table_turns tbody#list_table_body').append(tr);
}

$("#table_turns").DataTable({

    "columns": [
        { "data": "cliid", "autoWidth": true, className: "table-light" },
        { "data": "cliente", "autoWidth": true, className: "table-primary" },
        { "data": "tipo_servicio", "autoWidth": true, className: "table-light" },
        { "data": "cola", "autoWidth": true, className: "table-primary" },
        { "data": "posicion", "autoWidth": true, className: "table-light" },
        { "data": "status", "autoWidth": true, className: "table-primary" },
        { "data": "tiempo_espera", "autoWidth": true, className: "table-light" },

    ],
    "language": {
        "lengthMenu": "Mostrar _MENU_ registros",
        "zeroRecords": "No se encontraron resultados",
        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "sSearch": "Buscar:",
        "oPaginate": {
            "sFirst": "Primero",
            "sLast": "Último",
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "sProcessing": "Procesando...",
    },
    "width": "100%"
});
console.log(list);