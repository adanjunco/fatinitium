﻿var connection = new signalR.HubConnectionBuilder().withUrl("/hub").build();

connection.start().then(function () {
    setInterval(() => {
        $.ajax({
            type: 'GET',
            url: '/admin/turn/GetAll',
            success: function (data) {
                if (data != null) {
                    
                            $.each(data, function (i, v) {
                                var time_actually = new Date().toString();
                                $.each(v, function (indice, valor) {
                                    var time_turn = valor['time'].substr(11, 16);
                                    var time_now = time_actually.substr(16, 21);
                                    if (time_turn <= time_now) {
                                        var id_turn = valor['id'];
                                        $.ajax({
                                            url: `/admin/turn/Put/${id_turn}`,
                                            method: 'PUT',
                                            success: function (data) {
                                                if (data.success) {
                                                    toastr.success(data.message);
                                                    clearInterval();
                                                }
                                                else {
                                                    console.log('no enviado')
                                                }
                                            }
                                        });
                                    } else {
                                        console.log('turno vigente' + valor['id']);
                                    }
                                })
                            });
                        
                }
                else {
                    console.log("no hay data");
                }
            }
        });
    }, 2000);
    });

