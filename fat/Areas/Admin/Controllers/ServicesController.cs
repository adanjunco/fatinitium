﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAT.DataAccess.Data.Repository;
using FAT.Models;
using Microsoft.AspNetCore.Mvc;

namespace FAT.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ServicesController : Controller
    {
        private readonly IContainer _container;

        public ServicesController(IContainer container)
        {
            _container = container;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Services service)
        {
            if (ModelState.IsValid)
            {
                _container.Service.Add(service);
                _container.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(service);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Services services = new Services();
            services = _container.Service.Get(id);
            if (services == null)
            {
                return NotFound();
            }

            return View(services);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Services service)
        {
            if (ModelState.IsValid)
            {
                _container.Service.Update(service);
                _container.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(service);
        }

        

        #region call api
        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new {data = _container.Service.GetAll()});
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var objFromDb = _container.Service.Get(id);
            if(objFromDb == null)
            {
                return Json(new { success = false, message = "Error al borrar el servicio" });
            }

            _container.Service.Remove(objFromDb);
            _container.Save();
            return Json(new { success = true, message = "Servicio Borrado" });
        }

        #endregion
    }
}
