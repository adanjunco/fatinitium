﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAT.DataAccess.Data.Repository;
using Microsoft.AspNetCore.Mvc;

namespace FAT.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class QueuesController : Controller
    {
        private readonly IContainer _container;

        public QueuesController(IContainer container)
        {
            _container = container;
        }

        public IActionResult Index()
        {
            return View();
        }


        #region call api
        [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new { data = _container.Queue.GetAll() });
        }

        #endregion
    }
}
