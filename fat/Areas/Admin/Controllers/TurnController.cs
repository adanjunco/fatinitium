﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAT.Areas.User.Controllers;
using FAT.DataAccess.Data;
using FAT.DataAccess.Data.Repository;
using FAT.Models;
using FAT.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FAT.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TurnController : Controller
    {
        private readonly IContainer _container;
        private readonly ApplicationDbContext _context;

        public TurnController(IContainer container, ApplicationDbContext context)
        {
            _container = container;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create(int idService, string tittleService)
        {
            var queue_assign = 0;
            TempData["idService"] = idService;
            TempData["tittleService"] = tittleService;
            var queue_count_1 = _context.Turn.Where(q => q.QueueId == 1 && q.Status == 0).Count();
            var queue_count_2 = _context.Turn.Where(q => q.QueueId == 2 && q.Status == 0).Count();
            var time_queue_1 = _context.Queue.Where(t => t.id == 1).FirstOrDefault();
            var time_queue_2 = _context.Queue.Where(t => t.id == 2).FirstOrDefault();

            if (queue_count_1 * time_queue_1.time <= queue_count_2 * time_queue_2.time)
            {
                queue_assign = 1;
            }
            else
            {
                queue_assign = 2;
            }
            TempData["queueAssign"] = queue_assign.ToString();

            return View();
        }

        [HttpGet]
        public IActionResult GetTurns()
        {
            var getTurns = from turn in _context.Turn
                           join dataturn in _context.DataTurn
                           on turn.Id equals dataturn.id_turn
                           join serv in _context.Services
                           on turn.ServicesId equals serv.id
                           join queu in _context.Queue
                           on turn.QueueId equals queu.id
                           select new
                           {
                               cliid = turn.Cliid,
                               cliente = turn.CliName,
                               status = turn.Status,
                               posicion = dataturn.position,
                               tiempo_espera = dataturn.process_time,
                               tipo_servicio = serv.header,
                               cola = queu.name_queue

                           };


            JArray jArray = JArray.FromObject(getTurns.ToList());
            @TempData["ListTurn"] = JsonConvert.SerializeObject(getTurns.ToList());
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Turn turn)
        {
            if (ModelState.IsValid)
            {
                _container.Turn.Add(turn);
                _container.Save();

                var time = _context.Queue.Where(t => t.id == turn.QueueId).FirstOrDefault();
                var pending_shifts = _context.Turn.Where(p => p.Status == 0 && p.QueueId == turn.QueueId).Count();

                DateTime now = DateTime.Now;
                DataTurn dataTurn = new DataTurn();
                dataTurn.id_turn = turn.Id;
                dataTurn.id_queue = turn.QueueId;
                dataTurn.position = pending_shifts + 1;
                dataTurn.process_time = now.AddMinutes(pending_shifts * time.time);
                _context.DataTurn.Add(dataTurn);
                _context.SaveChanges();


                return Redirect("/");
            }
            return View(turn);
        }

        
            #region call api
            [HttpGet]
        public IActionResult GetAll()
        {
            return Json(new
            {
                data = from turn in _context.Turn
                       join data in _context.DataTurn
                       on turn.Id equals data.id_turn
                       where turn.Status == 0
                       select new
                       {
                           id = turn.Id,
                           time = data.process_time,
                           status = turn.Status
                       }
            });
        }

        


        [HttpPut]
        public IActionResult Put(Turn turn, int id)
        {
            
            turn.Status = 1;
            turn.Id = id;
            _container.Turn.Update(turn);
            _container.Save();

            return Json(new { success = true, message = "Actualizando status turno: " + id + " fue atendido"});
        }
        #endregion

    }

}