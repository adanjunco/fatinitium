﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace FAT.SignalR
{
    public class SignalRConnect : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public async Task NuevoTurno()
        {
            await Clients.All.SendAsync("NuevoTurno");
        }
    }
}
