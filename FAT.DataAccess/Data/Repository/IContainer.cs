﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data.Repository
{
    public interface IContainer : IDisposable
    {
        IServiceRepository Service { get; }
        ITurnRepository Turn { get; }
        IQueueRepository Queue { get; }
        IDataTurnRepository DataTurn { get; }

        void Save();
    }
}
