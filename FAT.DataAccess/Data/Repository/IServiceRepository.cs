﻿using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data.Repository
{
    public interface IServiceRepository : IRepository<Services>
    {
        IEnumerable<SelectListItem> GetListServices();

        void Update(Services services);
        
    }
}
