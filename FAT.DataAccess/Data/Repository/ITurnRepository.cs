﻿using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data.Repository
{
    public interface ITurnRepository : IRepository<Turn>
    {
        void Update(Turn turn);
        void Select(Turn turn);
    }
}
