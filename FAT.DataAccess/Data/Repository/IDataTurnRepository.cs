﻿using FAT.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data.Repository
{
    public interface IDataTurnRepository : IRepository<DataTurn>
    {
        void Update(Turn turn);
        void Select(Turn turn);
        object GetAll(object p);
    }
}
