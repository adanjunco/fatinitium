﻿using FAT.DataAccess.Data.Repository;
using FAT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FAT.DataAccess.Data
{
    public class QueueRepository : Repository<Queue>, IQueueRepository
    {
        private readonly ApplicationDbContext _db;

        public QueueRepository(ApplicationDbContext db) : base (db)
        {
            _db = db;
        }

        public IEnumerable<SelectListItem> GetListQueues()
        {
            return _db.Queue.Select(i => new SelectListItem()
            {
                Text = i.name_queue,
                Value = i.id.ToString()
            });
        }

        public void Update(Queue queue)
        {
            var objDb = _db.Queue.FirstOrDefault(s => s.id == queue.id);
            objDb.name_queue = queue.name_queue;
            objDb.time = queue.time;

            //_db.SaveChanges();
        }
    }
}
