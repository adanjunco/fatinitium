﻿using FAT.DataAccess.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace FAT.DataAccess.Data
{
    public class Container : IContainer
    {
        private readonly ApplicationDbContext _db;

        public Container(ApplicationDbContext db)
        {
            _db = db;
            Service = new ServicesRepository(_db);
            Queue = new QueueRepository(_db);
            Turn = new TurnRepository(_db);
            DataTurn = new DataTurnRepository(_db);

        }

        public IServiceRepository Service { get; private set; }
        public IQueueRepository Queue { get; private set; }
        public ITurnRepository Turn { get; private set; }
        public IDataTurnRepository DataTurn { get; private set; }



        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
      
    }
}
