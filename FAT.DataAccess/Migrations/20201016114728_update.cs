﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FAT.DataAccess.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "id",
                table: "Turn",
                newName: "Id");

            migrationBuilder.AddColumn<int>(
                name: "DataTurnid",
                table: "Turn",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Turn_DataTurnid",
                table: "Turn",
                column: "DataTurnid");

            migrationBuilder.AddForeignKey(
                name: "FK_Turn_DataTurn_DataTurnid",
                table: "Turn",
                column: "DataTurnid",
                principalTable: "DataTurn",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Turn_DataTurn_DataTurnid",
                table: "Turn");

            migrationBuilder.DropIndex(
                name: "IX_Turn_DataTurnid",
                table: "Turn");

            migrationBuilder.DropColumn(
                name: "DataTurnid",
                table: "Turn");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Turn",
                newName: "id");
        }
    }
}
