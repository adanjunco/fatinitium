﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FAT.DataAccess.Migrations
{
    public partial class deletedataturnidTableTurn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Turn_DataTurn_DataTurnid",
                table: "Turn");

            migrationBuilder.DropIndex(
                name: "IX_Turn_DataTurnid",
                table: "Turn");

            migrationBuilder.DropColumn(
                name: "DataTurnid",
                table: "Turn");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DataTurnid",
                table: "Turn",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Turn_DataTurnid",
                table: "Turn",
                column: "DataTurnid");

            migrationBuilder.AddForeignKey(
                name: "FK_Turn_DataTurn_DataTurnid",
                table: "Turn",
                column: "DataTurnid",
                principalTable: "DataTurn",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
